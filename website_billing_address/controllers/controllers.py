# -*- coding: utf-8 -*-
from odoo import http
from odoo.addons.website_sale.controllers.main import WebsiteSale  # Import the class
from odoo.http import request

class WebsiteBillingAddress(WebsiteSale):
    @http.route(['/shop/payment'], type='http', auth="public", website=True, sitemap=False)
    def payment(self, **post):
        """ Payment step. This page proposes several payment means based on available
        payment.acquirer. State at this point :

         - a draft sales order with lines; otherwise, clean context / session and
           back to the shop
         - no transaction in context / session, or only a draft one, if the customer
           did go to a payment.acquirer website but closed the tab without
           paying / canceling
        """
        res = super(WebsiteBillingAddress, self).payment(**post)
        order = res.qcontext['order']
        invoice_partner = order.partner_id.child_ids.filtered(lambda l: l.type == 'invoice')
        res.qcontext['invoice_partner'] = invoice_partner or False
        p = res.qcontext['partner']
        http.request.env['res.partner'].browse(p)
        return res

    def have_full_invoice_address(self,partner_id):
        for f in self._get_mandatory_billing_fields():
            if not partner_id[f]:
                return False
        return True

    @http.route(['/shop/checkout'], type='http', auth="public", website=True, sitemap=False)
    def checkout(self, **post):
        order = request.website.sale_get_order()

        redirection = self.checkout_redirection(order)
        if redirection:
            return redirection

        if order.partner_id.id == request.website.user_id.sudo().partner_id.id:
            return request.redirect('/shop/address')

        partner_id = order.partner_id
        invoice_address = partner_id.child_ids.filtered(lambda l: l.type == 'invoice')
        finvoice_add = self.have_full_invoice_address(invoice_address)
        fmain_add = self.have_full_invoice_address(partner_id)
        if (not finvoice_add) and (not fmain_add):
            return request.redirect('/shop/address?partner_id=%d' % order.partner_id.id)
        # for f in self._get_mandatory_billing_fields():
        #     if not partner_id[f]:
        #         if invoice_address and invoice_address[f]:
        #            partner_id[f] = invoice_address[f]
        #         else:
        #             return request.redirect('/shop/address?partner_id=%d' % order.partner_id.id)

        values = self.checkout_values(**post)

        if post.get('express'):
            return request.redirect('/shop/confirm_order')

        values.update({'website_sale_order': order})

        # Avoid useless rendering if called in ajax
        if post.get('xhr'):
            return 'ok'
        return request.render("website_sale.checkout", values)
